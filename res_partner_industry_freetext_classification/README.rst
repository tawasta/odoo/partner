.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/agpl-3.0-standalone.html
   :alt: License: AGPL-3

========================================
Partner Industry Freetext Classification
========================================

* Add 'Other Industry Classification' freetext field for partners
* Can be used to supplement the connection between partner and res.partner.industry model


Configuration
=============
* None needed

Usage
=====
* Just open the contact form

Known issues / Roadmap
======================
\-

Credits
=======

Contributors
------------
* Valtteri Lattu <valtteri.lattu@tawasta.fi>
* Timo Talvitie <timo.talvitie@tawasta.fi>

Maintainer
----------

.. image:: https://tawasta.fi/templates/tawastrap/images/logo.png
   :alt: Oy Tawasta OS Technologies Ltd.
   :target: https://tawasta.fi/

This module is maintained by Oy Tawasta OS Technologies Ltd.
